---
stages:
  - lint
  - prep simulation environment
  - oob-mgmt bringup
  - network bringup
  - provision simulation
  - test the simulation
  - cleanup simulation

lint:
  stage: lint
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - bash ./cldemo2/ci-common/linter.sh

prep:
  before_script:
    - PRIVATE_TOKEN="$API_KEY" gitlab-job-guard -c="^(master|dev*)$"
  stage: prep simulation environment
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - bash ./cldemo2/ci-common/prep.sh
  only:
    refs:
      - /^dev.*/
      - /^master$/

tc-build:
  stage: prep simulation environment
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  before_script:
    - if [ -z $TUNNEL_IP ]; then TUNNEL_IP="127.0.0.1"; fi
    - cp -r cldemo2/simulation simulation
  script:
    - cd simulation
    - python3 topology_converter.py cldemo2.dot -p libvirt -cmd -i $TUNNEL_IP --prefix simulation_${CI_PROJECT_NAME}_
  artifacts:
    expire_in: 1 week
    untracked: true
    paths:
      - simulation/
  only:
    refs:
      - /^dev.*/

build-oob-mgmt:
  stage: oob-mgmt bringup
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - bash ./cldemo2/ci-common/bring-up-oob-mgmt.sh
  artifacts:
    expire_in: 1 week
    untracked: true
    paths:
      - simulation/
  only:
    refs:
      - /^dev.*/
    changes:
      - automation/**/*
      - tests/*
      - .gitlab-ci.yml
      - cldemo2

test-start-script:
  stage: network bringup
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  before_script:
    - ln -s cldemo2/simulation simulation
  script:
    - cd simulation
    - python3 topology_converter.py cldemo2.dot -p libvirt -cmd --prefix simulation_${CI_PROJECT_NAME}_
    - cd ..
    - bash ./start-demo.sh
  after_script:
    - cd simulation
    - vagrant ssh oob-mgmt-server -c "sudo gitlab-runner register --non-interactive --url https://gitlab.com --registration-token $RUNNER_REG_TOKEN --description oob-mgmt-server:$CI_JOB_ID --tag-list $CI_PROJECT_NAME:oob-mgmt --executor shell"
    - sleep 60
  artifacts:
    expire_in: 1 week
    untracked: true
  only:
    refs:
      - /^master$/

setup-runner-on-oob-mgmt-server:
  stage: network bringup
  script:
    - cd simulation
    - vagrant ssh oob-mgmt-server -c "sudo gitlab-runner register --non-interactive --url https://gitlab.com --registration-token $RUNNER_REG_TOKEN --description oob-mgmt-server:$CI_JOB_ID --tag-list $CI_PROJECT_NAME:oob-mgmt --executor shell"
  only:
    refs:
      - /^dev.*/

build-network:
  stage: network bringup
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - bash ./cldemo2/ci-common/bring-up-network.sh
  after_script:
    - sleep 60
  artifacts:
    expire_in: 1 week
    untracked: true
    paths:
      - simulation/
  only:
    refs:
      - /^dev.*/

provision:
  stage: provision simulation
  script:
    - cd automation
    - ansible-playbook playbooks/deploy.yml -i inventories/pod1 --diff
  only:
    refs:
      - /^dev.*/
      - /^master$/
  tags:
    - dc_configs_vxlan_evpnsym:oob-mgmt

configure-netq-agents:
  stage: provision simulation
  script:
    - ansible server*:localhost -a "netq config add agent server $OPTA_SERVER_IP"
    - ansible leaf:spine:exit -a "netq config add agent server $OPTA_SERVER_IP vrf mgmt"
    - ansible exit:leaf:spine:server*:localhost -a "netq config restart agent"
  only:
    refs:
      - /^dev.*/
  tags:
    - dc_configs_vxlan_evpnsym:oob-mgmt

test-from-oob-server:
  stage: test the simulation
  script:
    - bash ./tests/oob-server-tests.sh
  only:
    refs:
      - /^dev.*/
  tags:
    - dc_configs_vxlan_evpnsym:oob-mgmt

test-from-netq-ts:
  stage: test the simulation
  script:
    - bash ./tests/netq-ts-tests.sh
  only:
    refs:
      - /^dev.*/
  tags:
    - dc_configs_vxlan_evpnsym:netq-ts

cleanup:
  stage: cleanup simulation
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - bash ./cldemo2/ci-common/cleanup.sh
  only:
    - /^dev.*$/
    - /^master$/
